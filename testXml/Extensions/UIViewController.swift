//
//  UIViewControllerExtension.swift
//  SlideMenuControllerSwift
//
//

import UIKit

extension UIViewController {
    
    var activityIndicatorTag: Int { return 999999 }
    
    
    
  func setNavigationTintColor()
  {
    
//    navigationController?.navigationBar.barTintColor = UIColor.green
    UINavigationBar.appearance().barTintColor = UIColor(red: 21.0/255.0, green: 151.0/255.0, blue: 224.0/255.0, alpha: 1.0)
    UINavigationBar.appearance().tintColor = UIColor.white

}
    
    
    func setStatusBarColor (color: UIColor)

    {
        UIApplication.shared.statusBarStyle = .lightContent
        
        //Change status bar color
        let statusBar: UIView = UIApplication.shared.value(forKey: "statusBar") as! UIView
        if statusBar.responds(to: #selector(setter: UIView.backgroundColor)) {
            statusBar.backgroundColor = color
        }
        

    }
    
    
    
    
    
    
    
    func setRightMenu()
    {
       
    }
    
    
    
    
    
    
    
    
    func setTitleForTopBar(viewname: String)
    {
        
        
       self.navigationItem.titleView =   Utility.titleView(stringText: viewname)
        
        
    }
    
    
    
    
    
    
    
    func setNavigationTransparent()
    {
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true

    }
    
    
    
    
    
    
    func setViewBackgroundColor(color: UIColor)
    {
        self.view.backgroundColor = color
    }
    
    // MARK - Methods for navigation items
    func setNavigationBarItem() {
//        self.addLeftBarButtonWithImage(UIImage(named: "LeftMenu")!)
//        //self.addRightBarButtonWithImage(UIImage(named: "ic_menu_black_24dp")!)
//        self.slideMenuController()?.removeLeftGestures()
//        //self.slideMenuController()?.removeRightGestures()
//        self.slideMenuController()?.addLeftGestures()
        // self.slideMenuController()?.addRightGestures()
    }
    
    func setNavigationBarWithBackButtonAndTitle(titleStr: NSString) {
        let leftButton: UIBarButtonItem = UIBarButtonItem(image: UIImage(named:"backButton"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(UIViewController.popBack))
        navigationItem.leftBarButtonItem = leftButton;
        self.title = titleStr as String
    }
    
    func removeNavigationBarItem() {
//        self.navigationItem.leftBarButtonItem = nil
//        //self.navigationItem.rightBarButtonItem = nil
//        self.slideMenuController()?.removeLeftGestures()
        //self.slideMenuController()?.removeRightGestures()
    }
    
    @objc public func popBack()
    {
        self.navigationController!.popViewController(animated: true)
    }
    
    // MARK - Methods for UIActivityIndicatorView
    func showActivityIndicator(
        style: UIActivityIndicatorViewStyle = .whiteLarge,
        
        location: CGPoint? = nil) {
        
            //Set the position - defaults to `center` if no`location`
            //argument is provided
            let loc = location ?? self.view.center
            
            //Ensure the UI is updated from the main thread
            //in case this method is called from a closure
            DispatchQueue.main.async(execute: {
                
                //Create the activity indicator
                let activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: style)
                //Add the tag so we can find the view in order to remove it later
                activityIndicator.tag = self.activityIndicatorTag
                activityIndicator.color = UIColor(red: 167.0/255.0, green: 52.0/255.0, blue: 51.0/255.0, alpha: 1.0)
                //Set the location
                activityIndicator.center = loc
                activityIndicator.hidesWhenStopped = true
                //Start animating and add the view
                activityIndicator.startAnimating()
                self.view.addSubview(activityIndicator)
            })
    }
    
    func stopActivityIndicator() {
        //Again, we need to ensure the UI is updated from the main thread!
        DispatchQueue.main.async(execute: {
            //Here we find the `UIActivityIndicatorView` and remove it from the view
            if let activityIndicator = self.view.subviews.filter(
                { $0.tag == self.activityIndicatorTag}).first as? UIActivityIndicatorView {
                    activityIndicator.stopAnimating()
                    activityIndicator.removeFromSuperview()
            }
        })
    }
    
    
    
     func showActionSheet(withMenu:[String], title:String)
    {
        
        let optionMenu = UIAlertController(title: nil, message: "Choose Option", preferredStyle: .actionSheet)

        
        
        
    }
    
    
    
    
    
    
    // MARK - Methods for custom alert
    func showCostumOkAlertWithTitle(alertTitle: String, alertMessage: String, tag: Int) {
        
        if tag == 0 {
            // Normal alert with no action for cancel
            let alert: UIAlertController = UIAlertController(title: alertTitle as String, message: alertMessage as String, preferredStyle: UIAlertControllerStyle.alert)
            
            
            if(Utility.getPlsitValueforKey(key: KEYS.LANG) as String == KEYS.ENG)
            {

            let cancelAction: UIAlertAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.cancel, handler:nil)
            alert.addAction(cancelAction)
            }
            else
            {
                let cancelAction: UIAlertAction = UIAlertAction(title: "موافق", style: UIAlertActionStyle.cancel, handler:nil)
                alert.addAction(cancelAction)
            }
            
            alert.popoverPresentationController?.sourceView = self.view
            alert.popoverPresentationController?.sourceRect = CGRect(x: self.view.bounds.size.width / 2, y: self.view.bounds.size.height / 2.0, width: 1.0, height: 1.0)
            self.present(alert, animated: true, completion: nil)
        }
        else if tag == 1 {
            // Normal alert with logout action for cancel
            let alert: UIAlertController = UIAlertController(title: alertTitle as String, message: alertMessage as String, preferredStyle: UIAlertControllerStyle.alert)
            
            let logoutAction: UIAlertAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: { (UIAlertAction) -> Void in
               // AppDelegate.DefaultManager.logout()
            })
            alert.addAction(logoutAction)
            
            alert.popoverPresentationController?.sourceView = self.view
            alert.popoverPresentationController?.sourceRect = CGRect(x: self.view.bounds.size.width / 2, y: self.view.bounds.size.height / 2.0, width: 1.0, height: 1.0)
            self.present(alert, animated: true, completion: nil)
        }
        else if tag == 2 {
            // Normal alert with pop action for cancel
            let alert: UIAlertController = UIAlertController(title: alertTitle as String, message: alertMessage as String, preferredStyle: UIAlertControllerStyle.alert)
            
            let defaultAction: UIAlertAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: { (UIAlertAction) -> Void in
                self.navigationController?.popViewController(animated: true)
            })
            alert.addAction(defaultAction)
            
            alert.popoverPresentationController?.sourceView = self.view
            alert.popoverPresentationController?.sourceRect = CGRect(x: self.view.bounds.size.width / 2, y: self.view.bounds.size.height / 2.0, width: 1.0, height: 1.0)
            self.present(alert, animated: true, completion: nil)
        }
        else if tag == 3 {
            // Normal alert with double pop action for cancel
            let alert: UIAlertController = UIAlertController(title: alertTitle as String, message: alertMessage as String, preferredStyle: UIAlertControllerStyle.alert)
            
            let defaultAction: UIAlertAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: { (UIAlertAction) -> Void in
                let viewControllers: [UIViewController] = self.navigationController!.viewControllers as [UIViewController];
                self.navigationController!.popToViewController(viewControllers[viewControllers.count - 3], animated: true);
            })
            alert.addAction(defaultAction)
            
            alert.popoverPresentationController?.sourceView = self.view
            alert.popoverPresentationController?.sourceRect = CGRect(x: self.view.bounds.size.width / 2, y: self.view.bounds.size.height / 2.0, width: 1.0, height: 1.0)
            self.present(alert, animated: true, completion: nil)
        }
        else if tag == 4 {
            // Alert with logout action + Cancel
            let alert: UIAlertController = UIAlertController(title: alertTitle as String, message: alertMessage as String, preferredStyle: UIAlertControllerStyle.alert)
            
            let defaultAction: UIAlertAction = UIAlertAction(title: "Yes", style: UIAlertActionStyle.default, handler: { (UIAlertAction) -> Void in
                //AppDelegate.DefaultManager.callLogoutService()
            })
            alert.addAction(defaultAction)
            
            let cancelAction: UIAlertAction = UIAlertAction(title: "No", style: UIAlertActionStyle.cancel, handler: nil)
            alert.addAction(cancelAction)
            
            alert.popoverPresentationController?.sourceView = self.view
            alert.popoverPresentationController?.sourceRect = CGRect(x: self.view.bounds.size.width / 2, y: self.view.bounds.size.height / 2.0, width: 1.0, height: 1.0)
            self.present(alert, animated: true, completion: nil)
        }
    }
    
}
