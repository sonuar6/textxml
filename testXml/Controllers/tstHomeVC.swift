//
//  tstHomeVC.swift
//  testXml
//
//  Created by Rahul kr on 10/07/18.
//  Copyright © 2018 Rahul kr. All rights reserved.
//

import UIKit

class tstHomeVC: UIViewController,MenuDelegate {
    
    
    
    
    @objc func menuPressed(sender:UIButton)
    {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let initialViewController = storyboard.instantiateViewController(withIdentifier: "Menu") as! tstMenuVC
        initialViewController.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        initialViewController.delegate = self
        let naigation = UINavigationController(rootViewController: initialViewController)
        self.present(naigation, animated: true, completion: nil)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        let button  = UIButton(type: .custom)
        if let image = UIImage(named:"menu.png") {
            button.setImage(image, for: UIControlState.normal)
        }
        button.frame = CGRect(x:0.0, y:0.0,width: 30.0, height: 30.0)
        button.addTarget(self, action: #selector(menuPressed), for: .touchUpInside)
        let barButton = UIBarButtonItem(customView: button)
        navigationItem.leftBarButtonItem = barButton

        

        // Do any additional setup after loading the view.
    }
    
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - MenuDelegate
    
    
    func selectedMenuItem(itemIndex: NSInteger) {
        
        if itemIndex == 1
        {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let initialViewController = storyboard.instantiateViewController(withIdentifier: "BannerView") as! tstBannerVC
            initialViewController.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
            let navigation = UINavigationController(rootViewController: initialViewController)
            self.present(navigation, animated: true, completion: nil)
        }
        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
