//
//  tstMenuVC.swift
//  testXml
//
//  Created by Apple on 7/10/18.
//  Copyright © 2018 Rahul kr. All rights reserved.
//




import UIKit
@objc protocol MenuDelegate
{
   @objc func selectedMenuItem(itemIndex:NSInteger)
}
class tstMenuVC: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    var delegate:MenuDelegate?
    let menuArray = ["Home","Banner","About"]

    @IBOutlet weak var menuTable: UITableView!
    
    @objc func cancelPressed(button:UIButton)
    {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
       let button  = UIButton(type: .custom)
        
        
        button.setTitle("Done", for: UIControlState.normal)
        button.setTitleColor(UIColor.red, for: UIControlState.normal)
        button.frame = CGRect(x:0.0, y:0.0,width: 50, height: 30.0)
        //        button.addTarget(self, action: #selector(menuPressed), forControlEvents: .TouchUpInside)
        button.addTarget(self, action: #selector(cancelPressed), for: .touchUpInside)
        let barButton = UIBarButtonItem(customView: button)
        navigationItem.leftBarButtonItem = barButton

        // Do any additional setup after loading the view.
    }

    
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        
        return self.menuArray.count
        
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "menuCell", for: indexPath)
        
        if self.menuArray.count>0
        {
            cell.textLabel?.text = self.menuArray[indexPath.row]
        }
        
        
        
        return cell
        
        
        
        
        
        
    }
    
    
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        self.dismiss(animated: true, completion: nil)

        self.delegate?.selectedMenuItem(itemIndex: indexPath.row)
        
    }
    
    
    
    
    
    
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        
        var x:NSInteger! = 0
        
        
        x  = 40
        
        
        
        
        
        return CGFloat(x)
    }
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
