//
//  tstBannerVC.swift
//  testXml
//
//  Created by Apple on 7/10/18.
//  Copyright © 2018 Rahul kr. All rights reserved.
//

import UIKit

class tstBannerVC: UIViewController,UITableViewDataSource,UITableViewDelegate{

    var banner:[BannerData]?
    var pageIndex:NSInteger = 0
    
    @IBOutlet weak var listTable: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setTitleForTopBar(viewname: "Banner")
        if Utility.isInternetAvailable() {
            self.fetchBanner()
            self.showActivityIndicator()
        } else {
            self.showCostumOkAlertWithTitle(alertTitle: AppTitle.APP_NAME, alertMessage: "Connectivity Error", tag: 0)
        }

        // Do any additional setup after loading the view.
    }
    
    
    
    func fetchBanner()  {
        
        
        
        let params = ["pi": String(pageIndex),"ps": "3","ln": "en"]
        
        
        print(params)
        
        
        testXMLAPIManager.getRequest(url: XMLApi.BASE, parameters: params)
        { (finished, statusCode, response) in
            var banners = [BannerData]()
            
            if let response = response, let jsonArray: Array = response["Banner"] as? [[String: AnyObject]]
            {
                print(jsonArray)
                
                
                
                
                for  listItem in jsonArray{
                    
                    banners.append(BannerData(response:listItem as [String : AnyObject] ))
                }
            }
            
            self.stopActivityIndicator()
            self.banner = banners
            
            self.listTable.reloadData()
        }
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return (banner != nil) ? banner!.count : 0
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        
        return 1
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        
        
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "bannerCell", for: indexPath) as! bannerTableCell
        
        if let banr = self.banner {
            let bnrData = banr[indexPath.section]
            cell.setCellData(data: bnrData)
            
       
            
        }
        
        cell.selectionStyle = UITableViewCellSelectionStyle .none
        
        
        cell.layer.shadowColor = UIColor .gray.cgColor
        cell.layer.shadowOffset = CGSize(width:1,height: 1)
        cell.layer.shadowOpacity = 1;
        cell.layer.shadowRadius = 1.0;
        cell.clipsToBounds = false;
        cell.layer.cornerRadius=3.0;
        
        
        
        cell.layer.borderColor = UIColor.white.cgColor
        cell.layer.borderWidth = 1.0
        
        return cell
        
        
        
        
        
        
    }
    
    
    
    
    
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?
    {
        let headerView = UIView(frame: CGRect(x: 0,y: 0,width: tableView.bounds.size.width,height: 5))
        
        headerView.backgroundColor = UIColor.clear
        
        return headerView
    }
    
    
    
    
    
    
   
    
    
    
    
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
