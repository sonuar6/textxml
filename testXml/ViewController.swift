//
//  ViewController.swift
//  testXml
//
//  Created by Rahul kr on 10/07/18.
//  Copyright © 2018 Rahul kr. All rights reserved.
//

import UIKit

class ViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    
    @IBOutlet weak var lanTable: UITableView!
    
    var button:UIButton?
    
    //let xmlData:XMLManager = XMLManager()
    
    
    
    
    
    
    @IBAction func goPressed(_ sender: Any) {
        
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let initialViewController = storyboard.instantiateViewController(withIdentifier: "HomeScreen")
        self.navigationController?.pushViewController(initialViewController, animated: true)
    }
    
    @objc func lanPressed(sender:UIButton)
    {
        if self.lanTable.isHidden {
            self.lanTable.isHidden  = false
        }
        else
        {
            self.lanTable.isHidden  = true

        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        print(XMLManager.books)
        
         button  = UIButton(type: .custom)

        
        self.button?.setTitle(Utility.getPlsitValueforKey(key: KEYS.LANG) as String, for: UIControlState.normal)
        self.button?.setTitleColor(UIColor.red, for: UIControlState.normal)
        button?.frame = CGRect(x:0.0, y:0.0,width: 120.0, height: 30.0)
        //        button.addTarget(self, action: #selector(menuPressed), forControlEvents: .TouchUpInside)
        button?.addTarget(self, action: #selector(lanPressed), for: .touchUpInside)
        let barButton = UIBarButtonItem(customView: button!)
        navigationItem.rightBarButtonItem = barButton
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1

    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        
        return XMLManager.books.count

        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)

        if XMLManager.books.count>0
        {
            let xml = XMLManager.books[indexPath.row]
            print(xml.bookTitle)
            cell.textLabel?.text = xml.bookTitle
        }
        
       
        
       return cell
        
        
        
        
        
        
    }
    
    
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let xml = XMLManager.books[indexPath.row]
        Utility.resetPlistValueForKey(key: KEYS.LANG, value: xml.bookTitle as NSString)
        button?.setTitle(xml.bookTitle, for: UIControlState.normal)


        self.lanTable.isHidden = true
        
    }
    

    
    
    
    
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        
        var x:NSInteger! = 0
        
        
        x  = 40
        
        
        
        
        
        return CGFloat(x)
    }

    
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

