//
//  bannerImgView.swift
//  testXml
//
//  Created by Apple on 7/10/18.
//  Copyright © 2018 Rahul kr. All rights reserved.
//

import Foundation
import UIKit
import Alamofire

class bannerImgView: UIImageView {

    
    
    /*Function to download and set image from URL */
    func setImageWithURL(url : String? , placeholder : UIImage?)  {
        
        if let placeholder = placeholder {
            self.image = placeholder
        }
        
        if var url = url, url.count > 1 {
            
            if url.hasPrefix("/Content") {
                url = XMLApi.BASE_CONTENT + url
                print(url)
            }
            
            Alamofire.request(URL(string :XMLApi.BASE_CONTENT + url)!,
                              method: .get,
                              parameters: nil,
                              encoding: URLEncoding.default, headers: nil).responseData{ (dataResponse) in
                                self.image = UIImage(data: dataResponse.data!, scale:1)
            }
        }
        
    }
    
    
    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
