//
//  XMLManager.swift
//  testXml
//
//  Created by Rahul kr on 10/07/18.
//  Copyright © 2018 Rahul kr. All rights reserved.
//

import UIKit




class XMLManager: NSObject,XMLParserDelegate {
    

    
    // Exists OR NOT
    
    
    //let xmlDataManager = XMLManager()

    static var books: [XMLData] = []
    static var titleArray = [String]()
    var eName: String = String()
    var bookTitle = String()
    var bookAuthor = String()
    
    
    
    class func isXmlFilePresent()-> Bool
    {
        if Bundle.main.url(forResource: "Data", withExtension: "xml") != nil {
            
            return true
            }
        return false

    }
    
    class func startParseData()
    {
        if let path = Bundle.main.url(forResource: "Data", withExtension: "xml") {
            if let parser = XMLParser(contentsOf: path) {

                let xmlManager = XMLManager()

                parser.delegate = xmlManager
                parser.parse()
            }
        }
    }

    // 1
    func parser(_ parser: XMLParser, didStartElement elementName: String, namespaceURI: String?, qualifiedName qName: String?, attributes attributeDict: [String : String]) {
         eName = elementName
        if elementName == "book" {
            bookTitle = String()
            bookAuthor = String()
        }
    }
    
    // 2
    func parser(_ parser: XMLParser, didEndElement elementName: String, namespaceURI: String?, qualifiedName qName: String?) {
        if elementName == "book" {
            
            let book = XMLData()
            book.bookTitle = bookTitle
            book.bookAuthor = bookAuthor
            
            XMLManager.books.append(book)
            XMLManager.titleArray.append(bookTitle)
        }
    }
    
    func parser(_ parser: XMLParser, foundCharacters string: String) {
        let data = string.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        if (!data.isEmpty) {
            print(eName)
            if eName == "title" {
                bookTitle += data
                print(bookTitle)
                print(data)

            } else if eName == "author" {
                bookAuthor += data
            }
        }
    }
    
    
    
   
    
}



