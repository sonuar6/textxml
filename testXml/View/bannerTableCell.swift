//
//  bannerTableCell.swift
//  testXml
//
//  Created by Apple on 7/10/18.
//  Copyright © 2018 Rahul kr. All rights reserved.
//

import UIKit
import AARatingBar


class bannerTableCell: UITableViewCell {
    
    
    @IBOutlet weak var lblCaption: UILabel!
    
    @IBOutlet weak var bImgView: bannerImgView!
    @IBOutlet weak var rateView: AARatingBar!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.lblCaption.text = ""
        self.rateView.value = 0
        // Initialization code
    }

    func setCellData(data: BannerData)
    {
        self.lblCaption.text = data.caption
        self.bImgView.setImageWithURL(url: data.b_image, placeholder: #imageLiteral(resourceName: "noImage"))
    }
    
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
