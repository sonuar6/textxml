//
//  appCharityConstants.swift
//  appCharity
//
//  Created by Arun on 2/2/17.
//  Copyright © 2017 Timeline. All rights reserved.
//

import Foundation
import UIKit
struct AppTitle {
    static let APP_NAME = "TestXML"
    static let APP_NAME_AR = "اقدر للخير"
    
}

struct KEYS
{
    
    static let USR_NAME  = "Username"
    static let PASSWORD  = "Password"
    static let ISLOGGEDIN = "LoggedIn"
    
    
    
    
    
    static let LANG = "Language"
    
    static let ENG = "en"
    
    static let ARB = "en"
    
    
    
    static let APP_NAME_AR = ""
    
    
    
    
    
    
}





struct Custom{
    
    
    static var fontTitle:UIFont! = UIFont(name: "AppleSDGothicNeo-Semibold", size: 14.0)
    
    static var fontViewHeader:UIFont! = UIFont(name: "AppleSDGothicNeo-Semibold", size: 11.0)
    
    
    
    static var fontHeader:UIFont! = UIFont(name: "AppleSDGothicNeo-Semibold", size: 16.0)
    
    
    static var fontNormal:UIFont! = UIFont(name: "AppleSDGothicNeo-Regular", size: 11.0)
    
    static var fontSmall:UIFont! = UIFont(name: "AppleSDGothicNeo-Semibold", size: 11.0)
    
    static var gColor:UIColor! = UIColor(red: 21.0/255.0, green: 151.0/255.0, blue: 224.0/255.0, alpha: 1.0)
    //UIColor(red: 94.0/255, green: 54.0/255, blue: 187.0/255, alpha: 1)
    static var statusColor:UIColor! = (UIColor (red: 167.0/255.0, green: 52.0/255.0, blue: 51.0/255.0, alpha: 1.0))
    
}




struct Path {
    static let DOCUMENTS = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as String
    static let TEMPDir = NSTemporaryDirectory()
}







struct XMLApi
    //198.154.255.97
{
    static let BASE = "http://www.shavemearabia.com/api/TestApi?"
    static let BASE_CONTENT = "http://www.shavemearabia.com/cms/contentfiles/BANNER/"


    
}

