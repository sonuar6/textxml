//
//  testXMLAPIManager.swift
//  testXml
//
//  Created by Rahul kr on 10/07/18.
//  Copyright © 2018 Rahul kr. All rights reserved.
//


import Foundation
import Alamofire

public typealias CompletionBlock = (_ finished : Bool, _ statusCode : Int?, _ response: [String :AnyObject]?) -> Void
public typealias didFinishedImageBlock = (_ finished : Bool, _ statusCode : Int?, _ image: UIImage?) -> Void
public typealias didFinishedAudioBlock = (_ finished : Bool, _ statusCode : Int?, _ audData: Data?) -> Void

class testXMLAPIManager: NSObject
{
    
    
    class func getRequest(url: String, parameters: Dictionary<String,String>?, completionHandler: CompletionBlock?) -> Void {
        
        print(" API Manager - get url :\(url) ")
        
        Alamofire.request(URL(string : url)!,
                          method: .get,
                          parameters: parameters,
                          encoding: URLEncoding.default,
                          headers: nil)
            .responseJSON { (response) in
                
                switch(response.result) {
                    
                case .success(let value):
                    if let data = value as? [String:AnyObject] {
                        completionHandler?(true, response.response?.statusCode, data)
                    }
                    break
                    
                case .failure(let error):
                    
                    if error._code == NSURLErrorTimedOut {
                        //Handle timeout here
                        completionHandler?(false, response.response?.statusCode, nil)
                    }
                    completionHandler?(true, response.response?.statusCode, nil)
                    
                    break
                    
                }
        }
    }
    
    
    
    
}
