//
//  Utility.swift
//  testXml
//
//  Created by Rahul kr on 10/07/18.
//  Copyright © 2018 Rahul kr. All rights reserved.
//

import UIKit
import SystemConfiguration

class Utility: NSObject {
    
    
    // Coder for storing sessions
    class func resetPlistValueForKey(key: String, value: NSString)
    {
        
        let defaults = UserDefaults.standard
        defaults.set(value, forKey:key)
    }
    
    // Coder for retrieving sessions

    class func getPlsitValueforKey(key: String) ->  NSString
    {
        
        let defaults = UserDefaults.standard
        
        var keyVal: NSString?
        
        keyVal = defaults.string(forKey: key) as NSString?
        if ((keyVal) != nil)
        {
            return keyVal!
        }
        else
        {
            return "0"
        }
        
    }
  
    // Internet Connectivity

    class func isInternetAvailable() -> Bool {
        
        var zeroAddress = sockaddr_in()
        zeroAddress.sin_len = UInt8(MemoryLayout<sockaddr_in>.size)
        zeroAddress.sin_family = sa_family_t(AF_INET)
        
        guard let defaultRouteReachability = withUnsafePointer(to: &zeroAddress, {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {
                SCNetworkReachabilityCreateWithAddress(nil, $0)
            }
        }) else {
            return false
        }
        
        var flags: SCNetworkReachabilityFlags = []
        if !SCNetworkReachabilityGetFlags(defaultRouteReachability, &flags) {
            return false
        }
        
        let isReachable = flags.contains(.reachable)
        let needsConnection = flags.contains(.connectionRequired)
        
        return (isReachable && !needsConnection)
    }

 
    
    // Setup for navigation header view

    
    class func titleView(stringText: String)-> UILabel
    {
        let label:UILabel = UILabel()
        label.frame = CGRect(x:0,y:0, width:200.0, height:40)
        label.backgroundColor = UIColor.clear
        label.numberOfLines=3;
        label.font = Custom.fontHeader
        label.textAlignment = NSTextAlignment .center
        label.textColor = UIColor.white
        //UIColor .init(colorLiteralRed: 51.0/255.0 , green: 156.0/255.0 , blue: 159.0/255.0, alpha: 1.0)
        
        
        label.text = stringText
        return label;
        
    }


}
