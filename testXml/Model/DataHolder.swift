//
//  DataHolder.swift
//  testXml
//
//  Created by Rahul kr on 10/07/18.
//  Copyright © 2018 Rahul kr. All rights reserved.
//

import Foundation

class XMLData {
    var bookTitle: String = String()
    var bookAuthor: String = String()
}

class BannerData: Codable {
    
    
    var b_barber_id: String?
    var b_image : String?
    var caption: String?
    var urlLink: String?
    var urlType: String?


    
    
    
   // Data model banner
    
    init(response : [String : AnyObject]?) {
        
        if let response = response {
            
            b_barber_id = response["barber_id"] as? String
            b_image = response["image"] as? String
            caption = response["caption"] as? String
            urlLink = response["urlLink"] as? String
            urlType = response["urlType"] as? String

            }
}
}

